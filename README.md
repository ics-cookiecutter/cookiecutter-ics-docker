# ICS docker image cookiecutter template

[Cookiecutter](https://github.com/audreyr/cookiecutter) template for ICS [Docker](https://www.docker.com) image.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet:

```bash
pip install --user cookiecutter
```

Generate a new repository to build the docker image.

```bash
cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-docker.git
```

As this is not easy to remember, you can add an alias in your `~/.bash_profile`:

```bash
alias ics-docker='cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-docker.git'
```

## Detailed instructions

The repository will be named `<name>`.

To create `foo`:

```bash
$ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-ics-docker.git
company [European Spallation Source ERIC]:
email [benjamin.bertrand@ess.eu]:
image_name [myimage]: foo
```

This creates the following project:

```bash
foo
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
└── README.md
```

Update the `Dockerfile` and `README.md`.

## License

BSD 2-clause license
