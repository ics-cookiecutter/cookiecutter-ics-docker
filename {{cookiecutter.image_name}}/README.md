# {{ cookiecutter.image_name }} docker image

{{ cookiecutter.image_name }} [docker] image for...

Docker pull command:

```bash
docker pull registry.esss.lu.se/ics-docker/{{ cookiecutter.image_name }}
```

[docker]: https://www.docker.com
