import re
import sys

IMAGE_NAME_REGEX = r"^[a-z][a-z0-9\-]+$"

image_name = "{{ cookiecutter.image_name }}"

if not re.match(IMAGE_NAME_REGEX, image_name):
    print(
        r'ERROR: "{}" is not a valid name! It should match "^[a-z][a-z0-9\-]+$"'.format(
            image_name
        )
    )
    sys.exit(1)
